#!/bin/perl

use strict;
use warnings;

my $serial_number = 1788;

my $highest_score = 0;
my $highest_x = 0;
my $highest_y = 0;

for (my $x = 1; $x <= 298; $x++) {
  for (my $y = 1; $y <= 298; $y++) {
    my $block_score = 0;
    for (my $block_x = 0; $block_x < 3; $block_x++) {
      for (my $block_y = 0; $block_y < 3; $block_y++) {
        my $cell_value = ((($x + $block_x + 10) * ($y + $block_y)) + $serial_number) * ($x + $block_x + 10);
        
        if (length($cell_value) >= 3) {
          $block_score += (substr($cell_value, length($cell_value) - 3, 1) - 5); #TODO: Make sure this works
        } else {
          $block_score -= 5;
        }
      }
    }

    if ($block_score > $highest_score) {
      $highest_score = $block_score;
      $highest_x = $x;
      $highest_y = $y;
    }
  }
}

print("Highest fuel score: $highest_score\n");
print("Best coords: " . $highest_x . "," . $highest_y . "\n");