#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day7_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

### Optimizations
#TODO: Only add used letters to the @alphabet
#TODO: Instead of looping the whole list at the end, remove completed letters from the @alphabet. When list is empty we're done.

my %steps;
my $result;
my @alphabet = split("", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
my @workers = (["", 0],["", 0],["", 0],["", 0],["", 0]);
my $seconds_to_next = 0;
my $total_time = 0;

foreach my $input_line (@lines) {
  $input_line  =~ /Step (.) must be finished before step (.) can begin./;
  my $part = $2;  #NOTE: I flipped these as I didn't read properly...
  my $depends = $1;
  
  $steps{$part}[0] = 0;
  push(@{$steps{$part}[1]}, $depends);  
}

print(join("",keys(%steps)) . "\n");

#JNSVKDYTXHRCGBOWAFQLMPZIUE
#OZQXTUYHEDMIAWFLBGPCKR

# Ugly fix...
#for (my $i = 0; $i < scalar(@alphabet); $i++) {
#  if (!defined $steps{$alphabet[$i]}) {
#    splice(@alphabet, $i, 1);
#  }
#}
#print(join("", @alphabet) . "\n");

my $is_done = 0;


while (!$is_done) {
  #NOTE: Debug-thing...
  #print("Sec: $seconds_to_next\n");
#  print("Res: $result\n");
#  print("Press return\n");
#  <STDIN>;

  $total_time += $seconds_to_next;
  my $lowest_seconds_to_next = 10000;
  foreach my $worker (@workers) {
    print("WorkerBef: " . @{$worker}[0] . "--" . @{$worker}[1] . "\n");
    if (@{$worker}[1] == 0) { next; }
    @{$worker}[1] -= $seconds_to_next;
    if ($lowest_seconds_to_next > @{$worker}[1] && @{$worker}[1] > 0) {
      $lowest_seconds_to_next = @{$worker}[1];
    }
    print("WorkerAft: " . @{$worker}[0] . "--" . @{$worker}[1] . "\n");
  }
  print("TotTime: $total_time\n\n");
  
  foreach my $worker (@workers) {
    if (@{$worker}[1] != 0) { next; }

    #print("DEBUG-01: " . @{$worker}[0] . "\n");
    if (@{$worker}[0] ne "") {
      #print("DEBUG-00: " . @{$worker}[0] . "\n");
      $result .= @{$worker}[0];
      $steps{@{$worker}[0]}[0] = 1;
      @{$worker}[0] = "";
    }
  }
  
  foreach my $worker (@workers) {
    if (@{$worker}[1] != 0) { next; }
    
    LETTER: foreach my $letter (@alphabet) {
      if (defined($steps{$letter}) && $steps{$letter}[0]) { next; }  # If done, keep going

      #print("DEBUG-02: " . $letter . "\n");
      
      foreach my $tempworker (@workers) {
        #print("DEBUG-04: " . @{$tempworker}[0] . "\n");
        if (@{$tempworker}[0] eq $letter) { next LETTER; }
      }
      
      my $can_run = 1;
      foreach my $dependency (@{$steps{$letter}[1]}) {
        if (!$steps{$dependency}[0]) {
          $can_run = 0;
        }
      }

      if ($can_run) {
        @{$worker}[0] = $letter;
        @{$worker}[1] = ord($letter) - 4; # Ends up as 60 + character number
        if ($lowest_seconds_to_next > @{$worker}[1]) {
          $lowest_seconds_to_next = @{$worker}[1];
        }
        #print("DEBUG-03: " . @{$worker}[1] . "\n");
        last;
      }
    }
  }
    
  
  $seconds_to_next = $lowest_seconds_to_next;

  $is_done = 1;
  foreach my $letter (@alphabet) {
    if (defined($steps{$letter}) && !$steps{$letter}[0]) {
      $is_done = 0;
      last;
    }
  }
}

print("Order: $result\n");
print("Time: $total_time\n");


