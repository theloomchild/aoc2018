#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day5_input.txt');
my (@characters) = split(//, <$input_file>);
close($input_file);

my @alphabet = split("", "QWERTYUIOPASDFGHJKLZXCVBNM");
#my @alphabet = split("", "ABCD");
my $best_letter = "1";
my $best_letter_length = 1000000;

foreach my $letter (@alphabet) {
  my @loop_characters = @characters;
  
  for (my $i = 0; $i < scalar(@loop_characters); $i++) {
    if (uc($loop_characters[$i]) eq $letter) {
      splice(@loop_characters, $i, 1);
      $i--;
    }
  }
  
#  print("DEBUG-02-letter: " . $letter . "\n");
#  print("DEBUG-02: " . join("",@loop_characters) . "\n");
  
  for (my $i = 0; $i < scalar(@loop_characters)-1; $i++) {
#    print("DEBUG-00a: " . join("",@loop_characters) . "\n");

    if (uc($loop_characters[$i]) eq uc($loop_characters[$i+1]) && $loop_characters[$i] ne $loop_characters[$i+1]) {
      splice(@loop_characters, $i, 2);
      if ($i > 0) { $i-=2; }  #Back up ONLY of we have something to back up to...
    }
#    print("DEBUG-00b: " . join("",@loop_characters) . "\n\n");
  }
  
#  print("DEBUG-01: " . $best_letter . "\n");
  
  if ($best_letter_length > scalar(@loop_characters)) {
    $best_letter = $letter;
    $best_letter_length = scalar(@loop_characters);
  }
}

print("Best size: " . $best_letter_length . "\n");
print("Best letter: " . $best_letter . "\n");