#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day6_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);


my @points = ();
my $area_size = 0;

foreach my $input_line (@lines) {
  # This can probably be made nicer, but I made a mistake and as this do work...
  my @temp = split(/, /, $input_line);
  push(@points, \@temp);
}

for (my $x = 50; $x < 350; $x++) {
  for (my $y = 50; $y < 350; $y++) {
    my $area_distance = 0;
    for (my $point = 0; $point < scalar(@points); $point++) {
      my @point = @{$points[$point]};
      $area_distance += abs($x - $point[0]) + abs($y - $point[1]);
    }
    
    if ($area_distance < 10000) {
      $area_size++;
      print("#");
    } else {
      print(".");
    }
  }
  print("\n");
}

print("Area size: " . $area_size . "\n");