#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day2_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

for (my $i = 0; $i < scalar(@lines); $i++) {
  my $input_word = $lines[$i];

  print "Input: " . $input_word . "\n";
  for (my $j = $i+1; $j < scalar(@lines); $j++) {
    my $non_matches = 0;
	my $test_word = $lines[$j];
	my $output_word = "";
    
	for (my $x = 0; $x < length($input_word); $x++) {
	  if (substr($input_word, $x, 1) ne substr($test_word, $x, 1)) {
	    $non_matches++;
		if ($non_matches > 1) { $x = 1000; } #Break out of loop, ugly style
	  } else {
	    $output_word .= substr($input_word, $x, 1);
	  }
	}
	
	if ($non_matches == 1) {
	  print "Output word: " . $output_word . "\n";
	  exit 1;
	}
	
  }
}

#print 'Checksum: ' . $count_double * $count_tripple;