#!/bin/perl

use strict;
use warnings;

#NOTE: This could be faster if we created the grid first. But I wanted to do the first part with a small memory foot print. And so with this, but if I want to do a "version 2"... ;)

my $serial_number = 1788;

my $highest_score = 0;
my $highest_x = 0;
my $highest_y = 0;
my $highest_size = 0;

for (my $current_size = 1; $current_size <= 300; $current_size++) {
  print("Current size: $current_size\n");
  for (my $x = 1; $x <= 301 - $current_size; $x++) {
    for (my $y = 1; $y <= 301 - $current_size; $y++) {
      my $block_score = 0;
      for (my $block_x = 0; $block_x < $current_size; $block_x++) {
        for (my $block_y = 0; $block_y < $current_size; $block_y++) {
          my $cell_value = ((($x + $block_x + 10) * ($y + $block_y)) + $serial_number) * ($x + $block_x + 10);
        
          if (length($cell_value) >= 3) {
            $block_score += (substr($cell_value, length($cell_value) - 3, 1) - 5); #TODO: Make sure this works
          } else {
            $block_score -= 5;
          }
        }
      }

      if ($block_score > $highest_score) {
        $highest_score = $block_score;
        $highest_x = $x;
        $highest_y = $y;
        $highest_size = $current_size;
      }
    }
  }
  print("Highest fuel score: $highest_score\n");
  print("Best coords: " . $highest_x . "," . $highest_y . "," . $highest_size . "\n\n");
}

print("\n\n\n\n");
print("Highest fuel score: $highest_score\n");
print("Best coords: " . $highest_x . "," . $highest_y . "," . $highest_size . "\n");