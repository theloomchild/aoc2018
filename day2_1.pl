#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day2_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my $count_double = 0;
my $count_tripple = 0;

foreach my $input_word (@lines) {
  print "Input: " . $input_word . "\n";
  my $has_double = 0;
  my $has_tripple = 0;
  for (my $i = 0; $i < length($input_word); $i++) {
    my $letter_count = 0;
	for (my $j = 0; $j < length($input_word); $j++) {
	  if (substr($input_word, $i, 1) eq substr($input_word, $j, 1)) {
	    $letter_count++;
	  }
	}
	#print 'DEBUG $letter_count: ' . $letter_count . "\n";
	if ($letter_count == 2) { $has_double = 1; }
	if ($letter_count == 3) { $has_tripple = 1; }
  }
  if ($has_double) { $count_double++; }
  if ($has_tripple) { $count_tripple++; }
  print "Counters: " . $count_double . " <> " . $count_tripple . "\n";
  print "Double: " . $has_double . "\n";
  print "Tripple: " . $has_tripple . "\n\n";
}

print 'Checksum: ' . $count_double * $count_tripple;