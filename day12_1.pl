#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day12_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my $initial_state = substr(shift(@lines), 15);
shift(@lines);

my @rules;

foreach my $input_line (@lines) {
  $input_line  =~ /(.)(.)(.)(.)(.) => (.)/;

  my (@rule) = ($1 eq "#" ? 1 : 0, $2 eq "#" ? 1 : 0, $3 eq "#" ? 1 : 0, $4 eq "#" ? 1 : 0, $5 eq "#" ? 1 : 0, $6 eq "#" ? 1 : 0);
  push(@rules, \@rule);
}

my @state_alpha;
my @state_bravo;

my $old_state = \@state_alpha;
my $new_state = \@state_bravo;

#Make array longer as we can't have negative indecis...indes...index-things...
for (my $i = 0; $i < 250; $i++) {
  @{$old_state}[$i] = 0;
  @{$new_state}[$i] = 0;
}

for (my $i = 0; $i < length($initial_state); $i++) {
  my $state = 0;
  if (substr($initial_state, $i, 1) eq "#") { $state = 1; }
  @{$old_state}[$i+100] = $state;
}

for (my $i = 0; $i < 20; $i++) {
  for (my $j = 2; $j < scalar(@{$old_state})-2; $j++) {
    for (my $k = 0; $k < scalar(@rules); $k++) {
#      print(@{$old_state}[$j-2] . " -- " . @{$rules[$k]}[0] . "\n");
      if (@{$old_state}[$j-2] == @{$rules[$k]}[0] && @{$old_state}[$j-1] == @{$rules[$k]}[1] && @{$old_state}[$j] == @{$rules[$k]}[2] && @{$old_state}[$j+1] == @{$rules[$k]}[3] && @{$old_state}[$j+2] == @{$rules[$k]}[4]) {
        @{$new_state}[$j] = @{$rules[$k]}[5];
      }
    }
  }
  
  print(join("", @{$new_state}) . "\n");

  if ($i % 2) {
    $old_state = \@state_alpha;
    $new_state = \@state_bravo;
  } else {
    $old_state = \@state_bravo;
    $new_state = \@state_alpha;
  }
}

my $result = 0;
for (my $i = 0; $i < scalar(@{$old_state}); $i++) {
  if (@{$old_state}[$i]) {
    $result += $i - 100;
  }
}

print("\n\n\n");
print("Result: $result\n");