#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day5_input.txt');
my (@characters) = split(//, <$input_file>);
close($input_file);

for (my $i = 0; $i < scalar(@characters)-1; $i++) {
#  print("DEBUG-00a: " . join("",@characters) . "\n");

  if (uc($characters[$i]) eq uc($characters[$i+1]) && $characters[$i] ne $characters[$i+1]) {
    splice(@characters, $i, 2);
    if ($i > 0) { $i-=2; }  #Back up ONLY of we have something to back up to...
  }
#  print("DEBUG-00b: " . join("",@characters) . "\n\n");
}

print("New size: " . scalar(@characters) . "\n");