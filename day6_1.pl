#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day6_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);


my @points = ();
my @inf_points = ();

foreach my $input_line (@lines) {
  # This can probably be made nicer, but I made a mistake and as this do work...
  my @temp = split(/, /, $input_line);
  push(@temp, (0, 0)); #area size, infinite area
  push(@points, \@temp);
}

#print(join(":", @{$points[0]}));

for (my $x = 0; $x < 500; $x++) {
  for (my $y = 0; $y < 500; $y++) {
    my $point_closest = "-1";
    my $point_distance = 9999;
    for (my $point = 0; $point < scalar(@points); $point++) {
      my @point = @{$points[$point]};
      my $distance = abs($x - $point[0]) + abs($y - $point[1]);
      
      if ($distance == $point_distance) {
        $point_closest = -1;
      }
      elsif ($distance < $point_distance) {
        $point_closest = $point;
        $point_distance = $distance;
      }
    }
    
    if ($point_closest > -1) {
      $points[$point_closest]->[2]++;
    }
    if ($x == 0 || $x == 499 || $y == 0 || $y == 499) {
      $points[$point_closest]->[3] = 1;
    }
  }
}

my $largest_area = 0;

for (my $i = 0; $i < scalar(@points); $i++) {
  my @point = @{$points[$i]};
  if ($point[3] == 0 && $largest_area < $point[2]) {
    $largest_area = $point[2];
  }
}

print("Largest area: " . $largest_area . "\n");