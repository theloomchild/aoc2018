#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day10_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my @points = ();
my $seconds = 0;

foreach my $input_line (@lines) {
  $input_line  =~ /position=< *([-0-9]+), *([-0-9]*)> velocity=< *([-0-9]*), *([-0-9]*)>/;
  my $pos_x = $1;
  my $pos_y = $2;
  my $velocity_x = $3;
  my $velocity_y = $4;
  
  my @temp = ($pos_x, $pos_y, $velocity_x, $velocity_y);
  
  splice(@points, scalar(@points), 0, \@temp);
}


my $loop = 1;
my $hight_max = 100000;
my $hight_min = -100000;

while ($loop) {
  my $loop_hight_max = 0;
  my $loop_hight_min = 0;
  
  foreach my $point (@points) {
    @{$point}[0] += @{$point}[2];
    @{$point}[1] += @{$point}[3];
    
    if (@{$point}[1] > $loop_hight_max) { $loop_hight_max = @{$point}[1]; }
    if (@{$point}[1] < $loop_hight_min) { $loop_hight_min = @{$point}[1]; }
  }

  if ($loop_hight_max - $loop_hight_min > $hight_max - $hight_min) {
    $loop = 0;
  }
  
  $seconds++;
  $hight_max = $loop_hight_max;
  $hight_min = $loop_hight_min;
}
# We did one step too many, go back...
foreach my $point (@points) {
  @{$point}[0] -= @{$point}[2];
  @{$point}[1] -= @{$point}[3];
  
  print("\e[" . (@{$point}[1] - 150) . ";" . (@{$point}[0] - 80) . "H");
  print("X");
}



print "\e[60;3H";
print 'Seconds: ' . ($seconds-1);

