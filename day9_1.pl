#!/bin/perl

use strict;
use warnings;

my $player_amount = 477;
my $max_marble = 7085100;

my @players = ();
my @marbles = (0);
my $current_marble = 0;

for (my $i = 0; $i < $player_amount; $i++) {
  $players[$i] = 0;
}

for (my $i = 1; $i <= $max_marble; $i++) {
  if ($i % 23 == 0) {
    $players[$i % $player_amount] += $i;
	my $remove_marble = $current_marble - 7;
	if ($remove_marble < 0) {
	  $remove_marble = scalar(@marbles) + $remove_marble;
	}
	$players[$i % $player_amount] += $marbles[$remove_marble];
	splice(@marbles, $remove_marble, 1);
	if ($remove_marble >= scalar(@marbles)) {
	  $remove_marble = 0;
	}
	$current_marble = $remove_marble;
  } else {
    my $insert_point = ($current_marble + 2) % (scalar(@marbles));
	splice(@marbles, $insert_point, 0, $i);
	$current_marble = $insert_point;
  }
  
  if ($i % 10000 == 0) {
    print("Progress: " . ($i / $max_marble) * 100 . "\n");
  }
}

my $best_score = 0;
foreach my $score (@players) {
  if ($score > $best_score) {
    $best_score = $score;
  }
}

print("Score: $best_score\n");