#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day4_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my @guard_actions = ();

foreach my $input_line (@lines) {
  my ($devnull, $input_datetime, $input_action) = split(/\[|\] /, $input_line);

  my @temp = ($input_datetime, $input_action);
  push(@guard_actions, \@temp);
}

#print("DEBUG: " . $guard_actions[0]->[1] . "\n");
#exit 0;

# Linear sort FTW! Much old-skool! So loopy!
for (my $i = 0; $i < scalar(@guard_actions); $i++) {
  for (my $j = $i+1; $j < scalar(@guard_actions); $j++) {
    #print("DEBUG: " . $i . " -- " . $j . " -- " .  "\n");
    my ($i_year, $i_month, $i_day, $i_hour, $i_minute) = split(/-| |:/, $guard_actions[$i]->[0]);  # [0] = $input_datetime above... (Too lazy to look up hashmaps/named arrays/whatever Perl calls it)
    my ($j_year, $j_month, $j_day, $j_hour, $j_minute) = split(/-| |:/, $guard_actions[$j]->[0]);
	
    if ($i_year . $i_month . $i_day . $i_hour . $i_minute > $j_year . $j_month . $j_day . $j_hour . $j_minute) {
      #TODO: Make sure this pivot works this way...
      my $temp = $guard_actions[$i];
      #print("DEBUG-00: " . $temp->[0] . "\n");
      $guard_actions[$i] = $guard_actions[$j];
      $guard_actions[$j] = $temp;
    }
  }
}

#foreach my $row (@guard_actions) {
#  print("DEBUG: " . $row->[0] . " - " . $row->[1] . "\n");
#}
#exit 0;

my %guards = ();
my $current_guard_id = undef;
my $current_guard_sleep = undef;
#TODO: Create array of guards. Each guard should have a "time grid" of all sixty minutes. Each minute starts at zero and this value increases by one if asleep at that time (so if asleep at the same time many nights, this will be high)
foreach my $guard_action (@guard_actions) {
  my $guard_action_substr = substr($guard_action->[1], 0, 5);
  if ($guard_action_substr eq "Guard") {
    $guard_action->[1] =~ m/[A-Za-z# ]([0-9]+)[A-Za-z# ]/;
    $current_guard_id = $1;
#    my @temparr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    if (!$guards{$current_guard_id}) { ($guards{$current_guard_id}) = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; }
    #TODO: Can a guard fall asleep before logs, or wake up after logs?
#    print("DEBUG-02: " . scalar(@{$guards{$current_guard_id}}) . "\n");
#    exit 0;
  }
	elsif ($guard_action_substr eq "falls") {
	  $current_guard_sleep = $guard_action->[0];
	}
	elsif ($guard_action_substr eq "wakes") {
    my ($sleep_year, $sleep_month, $sleep_day, $sleep_hour, $sleep_minute) = split(/-| |:/, $current_guard_sleep);
    my ($wake_year, $wake_month, $wake_day, $wake_hour, $wake_minute) = split(/-| |:/, $guard_action->[0]);  # [0] = $input_datetime above... (Too lazy to look up hashmaps/named arrays/whatever Perl calls it)
				   
    if ($sleep_hour == 23) { $sleep_minute = 0; }  #We start at midnight...
    if ($wake_hour == 1) { $wake_minute = 60; } #We end at 00:59
				   
    for (my $i = $sleep_minute; $i < $wake_minute; $i++) {
#      my $temp = $guards{$current_guard_id}[0];
#      print("$temp" . "\n");
      #print("DEBUG-03a: " . $guards{$current_guard_id}->[$i] . "\n");
      @{$guards{$current_guard_id}}[$i]++;  #TODO: This might hit undef stuff, crash?
      #print("DEBUG-03b: " . @{$guards{$current_guard_id}}[$i] . "\n");
    }
  }
}


#print("DEBUG-01: " . $guards[1523][10] . "\n");
#exit 0;

my $sleepiest_guard_id = -1;
my $sleepiest_guard_time = 0;
#for my $guard_key (keys %guards) {
#  my $guard_sleep_total = 0;
#  foreach my $minute_sleep (@{$guards{$guard_key}}) {
#    print("DEBUG-05: $guard_key " . $minute_sleep . "\n");
#    $guard_sleep_total += $minute_sleep;  #TODO: Will this work with "undef" sleeps?
#  }
#  
#  if ($guard_sleep_total > $sleepiest_guard_time) {
#    $sleepiest_guard_id = $guard_key;
#    $sleepiest_guard_time = $guard_sleep_total;
#  }
  
  #print("DEBUG-08: $guard_key $sleepiest_guard_id $sleepiest_guard_time \n");
#}

my $sleepiest_guard_minute = 0;
my $sleepiest_guard_minute_score = 0;
for (my $minute_sleep = 0; $minute_sleep < 60; $minute_sleep++) {
  foreach my $guard_key (keys %guards) {
    if (@{$guards{$guard_key}}[$minute_sleep] > $sleepiest_guard_minute_score) {
      $sleepiest_guard_minute_score = @{$guards{$guard_key}}[$minute_sleep];
      $sleepiest_guard_minute = $minute_sleep;
      $sleepiest_guard_id = $guard_key;
    }
  }
}


print("Guard ID: " . $sleepiest_guard_id . "\n");
print("Guard Time: " . $sleepiest_guard_time . "\n");
print("Guard Minute: " . $sleepiest_guard_minute . "\n");
print("Guard Result: " . $sleepiest_guard_id * $sleepiest_guard_minute . "\n");