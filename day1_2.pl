#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day1_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my $frequency = 0;
my @freq_seen = (0);

my $itteration = 0;

while (1==1) {
  print "Itteration: " . $itteration++ . "\n";
  print "List length: " . scalar(@freq_seen) . "\n\n";
  foreach my $freq_shift (@lines) {
    $frequency += $freq_shift;

#    print $frequency . ' <--> ' . $freq_shift . "\n";
	
    # TODO: Add check if Freq is in list
    foreach my $temp_val (@freq_seen) {
      if ($temp_val == $frequency) {
	    print "\n\n";
	    print 'Frequency: ' . $frequency;
	    exit 0;
	  }
    }
  
    push(@freq_seen, $frequency);
  }
}