#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day7_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

### Optimizations
#TODO: Only add used letters to the @alphabet
#TODO: Instead of looping the whole list at the end, remove completed letters from the @alphabet. When list is empty we're done.

my %steps;
my $result;
my @alphabet = split("", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

foreach my $input_line (@lines) {
  $input_line  =~ /Step (.) must be finished before step (.) can begin./;
  my $part = $2;  #NOTE: I flipped these as I didn't read properly...
  my $depends = $1;
  
  $steps{$part}[0] = 0;
  push(@{$steps{$part}[1]}, $depends);  
}

my $is_done = 0;

while (!$is_done) {
  foreach my $letter (@alphabet) {
    if (defined($steps{$letter}) && $steps{$letter}[0]) { next; }  # If done, keep going
    
    my $can_run = 1;
    foreach my $dependency (@{$steps{$letter}[1]}) {
#      print("DEBUG-02: " . $letter . "    " . $dependency . "\n");
      if (!$steps{$dependency}[0]) {
        $can_run = 0;
      }
    }
    
    if ($can_run) {
      $result .= $letter;
      $steps{$letter}[0] = 1;
      last;
    }
#    print("DEBUG-01: " . $letter . "\n");

  }
  
#  print("DEBUG-00: " . $result . "\n");
  
  #TODO: Loop and check if all is done!
  $is_done = 1;
  foreach my $letter (@alphabet) {
    if (defined($steps{$letter}) && !$steps{$letter}[0]) {
      $is_done = 0;
      last;
    }
  }
}

print("Order: $result\n");


