#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day8_input.txt');
#chomp(my @lines = <$input_file>);
my (@input) = split(" ", <$input_file>);
close($input_file);

#print(join(":",@input) . "\n");
# id-[childIDs],[metadata]

my @nodes = ();

sub read_data {
  my $children = shift(@input);
  my $metadata_count = shift(@input);
  
  my $id = scalar(@nodes);
  @{$nodes[$id]} = (); #Two empty arrays for child nodes and meta data
  @{$nodes[$id][0]} = ();
  @{$nodes[$id][1]} = ();
  
  for (my $i = 0; $i < $children; $i++) {
    my $child_id = read_data();
	push(@{$nodes[$id][0]}, $child_id);
  }
  
  for (my $i = 0; $i < $metadata_count; $i++) {
    my $metadata = shift(@input);
	push(@{$nodes[$id][1]}, $metadata);
  }
  
  return($id);
}

read_data();

sub get_node_value {
  my ($node_id) = @_;
  my $value = 0;

  if (scalar(@{$nodes[$node_id][0]}) == 0) {
    foreach my $metadata (@{$nodes[$node_id][1]}) {
	  $value += $metadata;
	}
  } else {
    foreach my $metadata (@{$nodes[$node_id][1]}) {
      if ($metadata > 0 && $metadata <= scalar(@{$nodes[$node_id][0]})) {
	    $value += get_node_value($nodes[$node_id][0][$metadata-1]);
	  }
	}
  }
  
  return $value;
}

my $sum_of_metadata = get_node_value(0);

#print(join(":", @{$nodes[scalar(@nodes)-1][0]}) . "\n");

print("Sum: " . $sum_of_metadata . "\n");



sub get_children {
  my $node_id = $_[0];
  my $level = $_[1];
  
  for (my $i = 0; $i < $level; $i++) {
    print("--");
  }
  print("$node_id  ");
  print(scalar(@{$nodes[$node_id][0]}) . "    ");
  print(join(":", @{$nodes[$node_id][1]}) . "\n");
  
  foreach my $child (@{$nodes[$node_id][0]}) {
    get_children($child, $level+1);
  }
}

#get_children(0,0);