#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day8_input.txt');
#chomp(my @lines = <$input_file>);
my (@input) = split(" ", <$input_file>);
close($input_file);

print(join(":",@input) . "\n");
# id-[childIDs],[metadata]

my @nodes = ();

sub read_data {
  my $children = shift(@input);
  my $metadata_count = shift(@input);
  
  my $id = scalar(@nodes);
  $nodes[$id] = (); #Two empty arrays for child nodes and meta data
  
  for (my $i = 0; $i < $children; $i++) {
    my $child_id = read_data();
	push(@{$nodes[$id][0]}, $child_id);
  }
  
  for (my $i = 0; $i < $metadata_count; $i++) {
    my $metadata = shift(@input);
	push(@{$nodes[$id][1]}, $metadata);
  }
  
  return($id);
}

read_data();

my $sum_of_metadata = 0;
for (my $i = 0; $i < scalar(@nodes); $i++) {
  my (@node) = @{$nodes[$i]};
  foreach my $metadata (@{$node[1]}) {
    $sum_of_metadata += $metadata;
  }
}

print("Sum: " . $sum_of_metadata . "\n")