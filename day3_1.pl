#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day3_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my @fabric_grid = ();

for (my $i = 0; $i < 1000; $i++) {
  $fabric_grid[$i] = ();
  for (my $j = 0; $j < 1000; $j++) {
    $fabric_grid[$i][$j] = 0;
  }
}

foreach my $input_line (@lines) {
  my ($devnull, $input_row, $input_coord_x, $input_coord_y, $input_width, $input_height) = split(/#|: | @ |x|,/, $input_line);

  for (my $i = $input_coord_x; $i < $input_coord_x + $input_width; $i++) {
    for (my $j = $input_coord_y; $j < $input_coord_y + $input_height; $j++) {
      $fabric_grid[$i][$j]++;
    }  
  }
}


my $overlap_count = 0;

for (my $i = 0; $i < 1000; $i++) {
  for (my $j = 0; $j < 1000; $j++) {
    if ($fabric_grid[$i][$j] > 1) { $overlap_count++; }
  }
}

print("Total overlap: " . $overlap_count . "\n");