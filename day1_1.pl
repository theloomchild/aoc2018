#!/bin/perl

use strict;
use warnings;

open(my $input_file, '<', './day1_input.txt');
chomp(my @lines = <$input_file>);
close($input_file);

my $frequency = 0;

foreach my $freq_shift (@lines) {
  $frequency += $freq_shift;
}

print $frequency;